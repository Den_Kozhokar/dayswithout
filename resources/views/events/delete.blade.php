@extends('layouts.app')

@section('content')
    <h1>Event is deleted</h1>
    <a href="{{action('EventsController@index')}}">Back</a>
@stop