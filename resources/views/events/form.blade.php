
<div class="form-group">
    {!! Form::label('text', 'Who:') !!}
    {!! Form::text('text', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('title', 'Event:') !!}
    {!! Form::text('title', null, ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('started_at', 'Started at:') !!}
    {!! Form::input('date', 'started_at', date('Y-m-d'), ['class'=>'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitBtn, ['class'=>'btn btn-primary form-control']) !!}
</div>