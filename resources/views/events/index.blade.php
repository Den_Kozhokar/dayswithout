@extends('layouts.app')

@section('content')
    <div class="text-center">
        <h1>Days without fuck up</h1>
        <br>
    </div>

    @foreach($events as $event)

          <div class="panel panel-default">
               <div class="panel-heading">
                   <h3 class="panel-title">
                       <span>
                           <a href="{{action('EventsController@show', [$event->id])}}"> {{$event->title}}</a>
                          
                       </span>
                       <span class="pull-right lable lable-info">
                           {{$event->started_at}}
                       </span>
                   </h3>
               </div>
          </div>
          <div class="panel-body">
                 <h3>
                     <div class="text-center">
                         <span class="pull-left">{{$event->text}}</span>
                        <span><b>{{$event->started_at->diffInDays()}} days </b></span>
                        <span class="pull-right"> without {{$event->title}}
                            {{--<a class="btn btn-info" href="{{action('HomeController@update', [$event->id])}}">
                                <i class="glyphicon glyphicon-ok"></i>
                            </a>--}}
                        </span>
                     </div>
                 </h3><hr>
                 <div class="pull-right">
                     <a class="btn btn-info" href="{{action('EventsController@edit', [$event->id])}}">
                         <i class="glyphicon glyphicon-pencil"></i>
                     </a>
                    {{--}} <a class="btn btn-danger" href="{{action('HomeController@edit', [$event->id])}}">
                         <i class="glyphicon glyphicon-trash"></i>
                     </a>--}}
                 </div>
          </div>
    @endforeach
    <div class="text-center">
        {!! $events->render()!!}
    </div>

    <div>
        <h1 class="h1">New event</h1>
        <hr>
        {!! Form::open(['url'=>'events']) !!}

        @include('events.form', ['submitBtn'=>'Add Event'])

        {!! Form::close() !!}

        @include('errors.list')
    </div>
    @stop



