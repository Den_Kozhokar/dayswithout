@extends('layouts.app')

@section('content')
    <h1 class="h1">New event</h1>
    <hr>
    {!! Form::open(['url'=>'events']) !!}

        @include('events.form', ['submitBtn'=>'Add Event'])

    {!! Form::close() !!}

    @include('errors.list')


@stop












































