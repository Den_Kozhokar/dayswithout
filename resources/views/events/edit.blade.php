@extends('layouts.app')

@section('content')
    <h1 class="h1">Edit: {!! $event->title !!}</h1>
    <hr>
    {!! Form::model($event, ['method'=>'PATCH', 'url'=>'events/' . $event->id]) !!}
    @include('events.form', ['submitBtn'=>'Edit Event'])


    {!! Form::close() !!}
    @include('errors.list')

@stop