@extends('layouts.app')

@section('content')
    <div class="text-center">
        <h1>Days without fuck up</h1>
        <br>
    </div>
    @foreach($events as $event)

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                       <span>
                            {{$event->user->name}}
                       </span>
                       <span class="pull-right lable lable-info">
                           {{$event->started_at}}
                       </span>
                </h3>
            </div>
        </div>
        <div class="panel-body">
            <div class="text-center">
                <h3>
                    <span class="pull-left">{{$event->text}}</span>
                    <span><b>{{$event->started_at->diffInDays()}} days</b></span>
                    <span class="pull-right">without {{$event->title}}</span>
                </h3><hr>
            </div>
        </div>
    @endforeach
    <div class="text-center">
        {!! $events->render()!!}
    </div>

@stop
