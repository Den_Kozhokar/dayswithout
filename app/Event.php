<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    protected $fillable = [
        'title',
        'text',
        'started_at'
    ];

    protected $dates = ['started_at'];

   /* public function scopePublished($query) {
        $query->where('published_at', '<=', Carbon::now());
    }

    public function scopeUnpublished($query) {
        $query->where('published_at', '>', Carbon::now());
    }*/

    public function setStartedAtAttribute($date) {
        $this->attributes['started_at'] = Carbon::createFromFormat('Y-m-d', $date);
    }

    public  function user() {
        return $this->belongsTo('App\User');
    }


}
