<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Event;
use App\Http\Requests;
use App\Http\Requests\EventRequest;
use Illuminate\Http\Response;
class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'main']);
    }

    public function index() {
        $events = Event::latest()->where('user_id',\Auth::user()->id )->paginate(5);
        return view('events.index', compact('events'));
    }

    public function main() {
        $events = Event::latest()->paginate(5);
        return view('main', compact('events'));
    }

    public function show(Event $event) {
        if (\Auth::user()->id == $event->user_id){
            return view('events.show', compact('event'));
    }
    else return redirect('events');
    }

    public function create() {

        return view('events.create');
    }

    public function store(EventRequest $request) {

        $event=new Event($request->all());
        \Auth::user()->Events()->save($event);
        return redirect('events');
    }

    public function edit(Event $event) {
        if (\Auth::user()->id == $event->user_id){
            return view('events.edit', compact('event'));
        }
        else return redirect('events');
    }

    public function update(Event $event, EventRequest $request) {

        $event->update($request->all());
        return redirect('events');
    }

    /*public function home() {
        return view('home');
    }*/

    public function delete(Event $event) {
        if (\Auth::user()->id == $event->user_id){
             $event->delete();//where('id', $event->id)->
            return view('delete');
        }
        else return redirect('events');
    }

    public function restart(Event $event) {
        $event->max_time=$event->started_at;
        $event->update(['started_at'=>Carbon::now()]);
        return redirect('events');
    }










}
