<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event;
//use App\Http\Requests;
use App\Http\Requests\EventRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    //restart started_at
    public function update(Event $event) {
        $event->max_time=$event->started_at;
        $event->update(['started_at'=>Carbon::now()]);
        return view('index');
    }

    public function edit(Event $event) {
        if (\Auth::user()->id == $event->user_id){
            $event->delete();//where('id', $event->id)->
            return view('index');
        }
        else return redirect('events');
    }



}
