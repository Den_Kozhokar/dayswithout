<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'EventsController@main');
Route::resource('events', 'EventsController');
Route::resource('home', 'HomeController');
/*Route::delete('events/{id}', ['as' => 'delete/{id}', function(App\Event $event)
{
   // $event->delete();//where('id', $event->id)->
   // return redirect('events');
}]);*/
/*Route::get('home', 'EventsController@home');
Route::get('events', 'EventsController@index');
Route::get('events/create', 'EventsController@create');

Route::post('events', 'EventsController@store');
Route::get('events/{id}/edit', 'EventsController@edit');
//Route::get('events', 'EventsController@update');
//Route::get('events', 'EventsController@delete');
//Route::get('events', 'EventsController@restart');
Route::get('events/{id}', 'EventsController@show');*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

//Route::resource('events', 'EventsController');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/*Route::get('foo', ['middleware'=>['auth', 'manager'], function()
{
    return 'This page only for managers';
}]);*/



